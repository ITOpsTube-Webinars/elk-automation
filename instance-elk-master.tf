resource "aws_instance" "elk" {
  ami = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.medium"
  vpc_security_group_ids = [ "${aws_security_group.elk_sg.id}" ]
  key_name = "${aws_key_pair.mykey.key_name}"
 
   provisioner "local-exec" {
     command = "sleep 120 && echo \"[elk]\n${aws_instance.elk.public_ip} ansible_connection=ssh ansible_ssh_user=centos ansible_ssh_private_key_file=mykey ansible_ssh_common_args='-o StrictHostKeyChecking=no'\" > elk-inventory &&  ansible-playbook -i elk-inventory install/elk.yml"
  }

  connection {
    user = "${var.INSTANCE_USERNAME}"
    private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
  }
tags {
    Name = "elk-master"
  }

}
output "elk_ip" {
    value = "${aws_instance.elk.public_ip}"
}
output "elk_END_URL" {
    value = "http://${aws_instance.elk.public_ip}"
}

