resource "aws_instance" "elk-client" {
  ami = "${lookup(var.AMIS, var.AWS_REGION)}"
  instance_type = "t2.micro"
  vpc_security_group_ids = [ "${aws_security_group.elk_sg.id}" ]
  key_name = "${aws_key_pair.mykey.key_name}"
 
   provisioner "local-exec" {
     command = "sleep 120 && echo \"[elk-client]\n${aws_instance.elk-client.public_ip} ansible_connection=ssh ansible_ssh_user=centos ansible_ssh_private_key_file=mykey ansible_ssh_common_args='-o StrictHostKeyChecking=no'\" > elk-client-inventory &&  ansible-playbook -i elk-client-inventory install/elk-client.yml --extra-vars 'elk_server=${aws_instance.elk.public_ip}'"
  }

  connection {
    user = "${var.INSTANCE_USERNAME}"
    private_key = "${file("${var.PATH_TO_PRIVATE_KEY}")}"
  }
tags {
    Name = "elk-client"
  }

}

